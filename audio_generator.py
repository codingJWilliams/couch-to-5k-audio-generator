from glob import glob
from pydub import AudioSegment
from pydub.playback import play
import random, json, subprocess

print("Loading speech...")
message_segs = {}
for mp3_file in glob("audio/*.wav"):
    proper_filename = mp3_file.split("/")[1].split(".")[0]
    message_segs[proper_filename] = AudioSegment.from_mp3(mp3_file)

print("Loading music...")
playlist_queue = [[mp3_file, AudioSegment.from_mp3(mp3_file)] for mp3_file in glob("music/*.mp3")]
random.shuffle(playlist_queue)

def get_next_x_seconds_of_audio(seconds: int, smooth_in = True) -> AudioSegment:
    global playlist_queue

    output_audio = AudioSegment.empty()
    while True:
        next_song = playlist_queue[0]
        playlist_queue = playlist_queue[1:] 
        remaining_seconds = seconds - len(output_audio)/1000
        if (len(next_song[1])/1000) > remaining_seconds:
            # If the next song in the queue is longer than the amount of remaining seconds
            output_audio = output_audio.append(next_song[1][:remaining_seconds*1000], 0)
            # print("Using part of", next_song[0])
            playlist_queue = [[next_song[0], next_song[1][(remaining_seconds*1000)+1:]]] + playlist_queue
            break
        else:
            print("   - Consumed", next_song[0])
            output_audio = output_audio.append(next_song[1], 0)
    if smooth_in:
        output_audio = output_audio.fade_in(500)
        output_audio = output_audio.fade_out(500)
    return output_audio

with open("./timings.json") as f:
    timings = json.load(f)

for i, week_data in enumerate(timings):
    week = i+1
    print(f"Processing week {week}", end="", flush=True)

    aud = AudioSegment.empty()
    aud += message_segs['warmup300s']
    aud += get_next_x_seconds_of_audio(300)

    message_timestamps = []
    for activity in week_data:
        audio_name = f"{activity['task']}{activity['time']}s"
        start_time = len(aud)
        aud += message_segs[audio_name]
        message_timestamps += [{ "start": start_time, "end": len(aud) }]
        aud += get_next_x_seconds_of_audio(activity['time'])
        print(".", end="", flush=True)
    
    
    aud += message_segs['warmdown300s']
    aud += get_next_x_seconds_of_audio(300)

    halfway_point = len(aud) / 2
    for ts in message_timestamps:
        if halfway_point > (ts['start'] - (5*1000)) and halfway_point < (ts['end'] + (5*1000)):
            halfway_point = ts['start'] - (10*1000)
    
    aud = aud[:halfway_point].fade_out(500) + message_segs['halfway'] + aud[halfway_point:].fade_in(500)

    with open(f"./output/week{week}.wav", "wb") as f:
        aud.export(f, format="wav")
    
    subprocess.call(f"lame --preset insane ./output/week{week}.wav", shell=True)

    print("", flush=True)
    print("Length: ", len(aud))