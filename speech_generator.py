'''
  For more samples please visit https://github.com/Azure-Samples/cognitive-services-speech-sdk 
'''

import azure.cognitiveservices.speech as speechsdk
from conf import speech_key, service_region

# Creates an instance of a speech config with specified subscription key and service region.

speech_config = speechsdk.SpeechConfig(subscription=speech_key, region=service_region)
# Note: the voice setting will not overwrite the voice element in input SSML.
speech_config.speech_synthesis_voice_name = "en-GB-LibbyNeural"

phrases = {
    "warmup300s": "Briskly walk for 5 minutes to warm up",
    "warmdown300s": "The run is now over. Continue walking for 5 minutes to cool down",
    "run60s": "Sixty seconds of running starts... now",
    "run90s": "Ninety seconds of running starts... now",
    "run180s": "Three minutes of running starts... now",
    "run300s": "Five minutes of running starts... now",
    "run480s": "Eight minutes of running starts... now",
    "run1500s": "Twenty-five minutes of running starts... now",
    "run1680s": "Twenty-eight minutes of running starts... now",
    "run1800s": "Thirty minutes of running starts... now",
    "walk90s": "Stop running. Walk for ninety seconds to cool off.",
    "walk120s": "Stop running. Walk for two minutes to cool off.",
    "walk180s": "Stop running. Walk for three minutes to cool off.",
    "walk150s": "Stop running. Walk for two and a half minutes to cool off.",
    "halfway": "You are halfway.",
}


for key, phrase in phrases.items(): 
    text = phrase

    # use the default speaker as audio output.
    audio_config = speechsdk.audio.AudioOutputConfig(filename=f"./audio/{key}.wav")

    speech_synthesizer = speechsdk.SpeechSynthesizer(speech_config=speech_config, audio_config=audio_config)

    result = speech_synthesizer.speak_text_async(text).get()

    # Check result
    if result.reason == speechsdk.ResultReason.SynthesizingAudioCompleted:
        print("Speech synthesized for text [{}]".format(text))
    elif result.reason == speechsdk.ResultReason.Canceled:
        cancellation_details = result.cancellation_details
        print("Speech synthesis canceled: {}".format(cancellation_details.reason))
        if cancellation_details.reason == speechsdk.CancellationReason.Error:
            print("Error details: {}".format(cancellation_details.error_details))

